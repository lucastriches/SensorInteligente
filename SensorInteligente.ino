/* Lucas Marcon Trichez
*
* Este programa envia os dados coletados do DHT11 para a plataforma de IoT
* thingspeak.com
* Data: 16 de Setembro de 2015
* Author: Pedro Minatel
* Website: http://pedrominatel.com.br
*/
 
//Include da lib de Wifi do ESP8266
#include <ESP8266WiFi.h>
//Include da lib do sensor DHT11 e DHT22
#include "DHT.h"
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <ESP8266mDNS.h>
//#define BLYNK_PRINT Serial    // Comment this out to disable prints and save space

//#define BLYNK_ENABLE
//#include <BlynkSimpleEsp8266.h>

//#define WIFIMANAGER_ENABLE
//#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic


#define OTA_ENABLE
#include <ArduinoOTA.h>
 
//Define do pino a ser utilizado no ESP para o sensor = GPIO4
#define DHT_DATA_PIN 2 //2=D4
#define RELAY_PIN 0
#define DHTTYPE DHT22

#define INTERVALO_ENVIO 6000
#define MEDIA_LEITURAS 50

 
//Configuracao WIFI
#define APP_NAME "Sensor-Inteligente"
// Definir a Rede Wifi
//const char* ssid = "hdlfpolis";
//Definir a senha da rede WiFi
//const char* password = "aquinoissurfa";


 
//Configuracao ThingSpeak
//Colocar a API Key para escrita neste campo
//Ela e fornecida no canal que foi criado na aba API Keys
String apiKey = "PEIFPC5YTH6VFVDY"; // Sensor 
//String apiKey = "MEGZT8MTD6DUXDI9"; // Sensor 2
//#define SENSOR_2 // envia para os fields 3 e 4 ao inves de 1 e 2

//Configuracao PushInBox
#define NOTIFICATION_ENABLE
#define NOTIFICATION_OVER 28
char device_id[] = "v9FCAD4DAFEFA932";

//Configuracao Blynk
#ifdef BLINK_ENABLE
//Configuracao Blynk (Chave AUtenticacao)
char blynk_auth[] = "8353e1c6ee6d4a78b2d0e26db2dc9693";
#endif



// Variaveis do Projeto
float umidade;
float umidade_media = 50;
float temperatura;
float temperatura_media = 20;
float temperatura_calibracao = -3;
float humidade_calibracao = 5;
int leituras = 0;
unsigned long tempo_leitura = 0;

//Objetos 
DHT dht(DHT_DATA_PIN, DHTTYPE);
WiFiClient client2; // ThinkSpeak
WiFiClient client3; // PushInBox
#ifdef WIFIMANAGER_ENABLE
WiFiManager wifiManager;
#endif
 
void setup() {
  //Configura��o da UART
  Serial.begin(9600);
  pinMode(RELAY_PIN,OUTPUT); 
  digitalWrite(RELAY_PIN,LOW);
  //Inicia o WiFi
//  wifiManager.resetSettings();
 //  WiFi.disconnect(); 
 //  ESP.eraseConfig();
 //  ESP.reset();
  Serial.print("Conectando ao WiFi-");
  Serial.print("MAC: ");
  Serial.println(WiFi.macAddress());
  
  setup_smartconfig();
   
#ifdef WIFIMANAGER_ENABLE
  // Solicita configuracao de WiFi
   wifiManager.autoConnect(APP_NAME);
 //  wifiManager.autoConnect(APP_NAME, "sensor1234");
   wifiManager.setDebugOutput(false);
#endif
   
#ifdef BLINK_ENABLE
   Blynk.config(blynk_auth);
#endif   

#ifdef OTA_ENABLE
   ArduinoOTA.setHostname(APP_NAME);
   ArduinoOTA.begin();
#endif
  dht.begin();
 
  //Logs na porta serial
  Serial.println("");
  Serial.print("Conectado na rede ");
//  Serial.println(ssid);
  Serial.print("IP: ");
  Serial.println(WiFi.localIP());
}
 
//Loop principal
void loop() {

  le_dht();
#ifdef BLINK_ENABLE
  Blynk.run();
#endif  
#ifdef OTA_ENABLE
  ArduinoOTA.handle();
#endif  
}

//Configuracao via APP
void setup_smartconfig(){

  WiFi.mode(WIFI_AP_STA);
 // WiFi.softAP("ESP_C0BD5E");
  /* start SmartConfig */
  WiFi.beginSmartConfig();

  /* Wait for SmartConfig packet from mobile */
  Serial.println("Waiting for SmartConfig.");
  while (!WiFi.smartConfigDone()) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("SmartConfig done.");

    /* Wait for WiFi to connect to AP */
  Serial.println("Waiting for WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
}

void le_dht() {
int tentativas;
  
  if ((millis() >= (tempo_leitura + INTERVALO_ENVIO)) || (tempo_leitura == 0)) {
    tempo_leitura = millis(); 
  }
  else return;
  //Leitura de umidade
  for (tentativas=0;tentativas<3;tentativas++){ 
    umidade = dht.readHumidity() + humidade_calibracao;
  //Leitura de temperatura
    temperatura = dht.readTemperature() + temperatura_calibracao;
    //Se leitura valida prossegue
    if (!(isnan(umidade) || isnan(temperatura))) {
      break;
    }
    delay(50);
  }
  if (isnan(umidade) || isnan(temperatura)) {
     Serial.println("Erro ao ler o sensor!");
     return;
  }
  Serial.print(" Temperatura: ");
  Serial.print(temperatura);
  Serial.print(" Umidade: ");
  Serial.println(umidade);
  

  leituras++;
  temperatura_media += temperatura;
  umidade_media += umidade;
  if (leituras >= MEDIA_LEITURAS) {
    leituras = 1;
    temperatura_media = temperatura_media / MEDIA_LEITURAS;
    umidade_media = umidade_media / MEDIA_LEITURAS;
    Serial.print(" Temperatura Media: ");
    Serial.print(temperatura_media);
    Serial.print(" Umidade media: ");
    Serial.println(umidade_media);
    envia_nuvem();   
#ifdef NOTIFICATION_ENABLE
    if (temperatura_media > NOTIFICATION_OVER){
      sendToPushingBox(device_id);
    }
#endif    
  }
}

void envia_nuvem(){
#ifdef BLINK_ENABLE
  Blynk.disconnect(); // Desconecta Bliack para evitar erros no envio para nuvem
#endif
  delay(100);
  //Inicia um client TCP para o envio dos dados
  if (client2.connect("api.thingspeak.com",80)) {
#ifdef SENSOR_2  
    String postStr = apiKey;
           postStr +="&amp;field3=";
           postStr += String(temperatura_media);
           postStr +="&amp;field4=";
           postStr += String(umidade_media);
           postStr += "\r\n\r\n";
#else
    String postStr = apiKey;
           postStr +="&amp;field1=";
           postStr += String(temperatura_media);
           postStr +="&amp;field2=";
           postStr += String(umidade_media);
           postStr += "\r\n\r\n";
#endif
 
     client2.print("POST /update HTTP/1.1\n");
     client2.print("Host: api.thingspeak.com\n");
     client2.print("Connection: close\n");
     client2.print("X-THINGSPEAKAPIKEY: "+apiKey+"\n");
     client2.print("Content-Type: application/x-www-form-urlencoded\n");
     client2.print("Content-Length: ");
     client2.print(postStr.length());
     client2.print("\n\n");
     client2.print(postStr);
 
     //Logs na porta serial
     Serial.println("Dados Enviados para ThingSpeak.");
  }
  delay(200);
  client2.stop();
#ifdef BLINK_ENABLE
  Blynk.connect();
#endif  
 
}

void sendToPushingBox(char devid[]){
#ifdef BLINK_ENABLE
  Blynk.disconnect(); // Desconecta Bliack para evitar erros no envio para nuvem
#endif
 
  client3.stop(); 
  if(client3.connect("api.pushingbox.com", 80)) { 
    Serial.println("connected pushinbox");
    Serial.println("sendind request");
    client3.print("GET /pushingbox?devid=");
    client3.print(devid);
    client3.println(" HTTP/1.1");
    client3.print("Host: ");
    client3.println("api.pushingbox.com");
    client3.println("User-Agent: Arduino");
    client3.println();
  } 
  else { 
     Serial.println("PushInBox connection failed"); 
  } 
#ifdef BLINK_ENABLE
  Blynk.connect();
#endif  
}


#ifdef BLINK_ENABLE

// Virtual handlers for our widgets...

BLYNK_READ(V0) {
  Blynk.virtualWrite(0,temperatura);
}

BLYNK_READ(V1) {
  Blynk.virtualWrite(1,umidade);
}

#endif

